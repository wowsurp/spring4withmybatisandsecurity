<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>
<table>
		<thead>
			<tr>
				<th>제목</th>
				<th>내용</th>
				<th>작성자</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${contentsList}" var="contents">
				<tr>
					<td>${contents.title}</td>
					<td>${contents.contents}</td>
					<td>${contents.userNm}</td>
				</tr>
			</c:forEach>
		</tbody>			
	</table>
<P>  The time on the server is ${serverTime}. </P>
</body>
</html>