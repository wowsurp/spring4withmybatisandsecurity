package com.brain.mybatis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.brain.mybatis.dto.ContentsDTO;

@Repository
public class ContentsDAOImpl implements ContentsDAO {
	
	@Autowired
	private SqlSession sqlSession;
	
	private static final String Namespace = "com.brain.mybatis.mapper.contentsMapper";
	
	@Override
	public List<ContentsDTO> selectContents() throws Exception {
		return sqlSession.selectList(Namespace+".selectContents");
	}

}
