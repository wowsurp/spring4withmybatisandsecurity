package com.brain.mybatis.dao;

import java.util.List;

import com.brain.mybatis.dto.ContentsDTO;

public interface ContentsDAO {
	public List<ContentsDTO> selectContents() throws Exception;
}
