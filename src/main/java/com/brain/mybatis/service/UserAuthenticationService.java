package com.brain.mybatis.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.brain.mybatis.dto.UserDetailsDTO;


public class UserAuthenticationService implements UserDetailsService {

	private SqlSessionTemplate sqlSession;

	public UserAuthenticationService(SqlSessionTemplate sqlSession) {
		this.sqlSession = sqlSession;
	}

	@Override
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		Map<String, Object> user = sqlSession.selectOne("com.brain.mybatis.mapper.memberMapper.selectLoginUser", userId);
		if (user == null) {
			throw new UsernameNotFoundException(userId);
		}
		List<GrantedAuthority> gas = new ArrayList<GrantedAuthority>();

		String role = null;
		
		if ((	user.get("role") == null  					|| 
				user.get("role").toString().length() == 0 	|| 
				user.get("role").equals("ROLE_ANONYMOUS")		)) {
			
			role = "ROLE_USER";
			
		} else{
			role = user.get("role").toString();
		}
		
		gas.add(new SimpleGrantedAuthority(role));
		
		return new UserDetailsDTO(user.get("userId").toString(), user.get("userNm").toString(),
				user.get("pwd").toString(), true, true, true, true, gas, user.get("email").toString());
	}

	@Bean
	public Md5PasswordEncoder passwordEncoder() throws Exception {
		return new Md5PasswordEncoder();
	}
	
}
