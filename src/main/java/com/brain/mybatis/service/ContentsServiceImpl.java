package com.brain.mybatis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brain.mybatis.dao.ContentsDAO;
import com.brain.mybatis.dto.ContentsDTO;

@Service
public class ContentsServiceImpl implements ContentsService {

	@Autowired
	private ContentsDAO dao;

	@Override
	public List<ContentsDTO> selectContents() throws Exception {
		return dao.selectContents();
	}
}
