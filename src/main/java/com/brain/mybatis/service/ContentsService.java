package com.brain.mybatis.service;

import java.util.List;

import com.brain.mybatis.dto.ContentsDTO;

public interface ContentsService {
	public List<ContentsDTO> selectContents() throws Exception;
}
